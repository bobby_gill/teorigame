module MembershipsHelper
  def paypal_url(amount, description, paypal_invoice_id, return_path)
    values = {
        business: "mar.jen12044-facilitator@gmail.com",
        cmd: "_xclick",
        upload: 1,
        return: "#{Rails.application.secrets.app_host}/#{return_path}",
        invoice: paypal_invoice_id,
        amount: amount,
        item_name: description,
        item_number: 1,
        quantity: '1',
        notify_url: "#{Rails.application.secrets.app_host}/hook?"
    }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end
end
