module QuizzesHelper
	def get_quiz_level(quiz_id)
    if current_user.present?
      scorecard = current_user.scorecards.find_by_quiz_id quiz_id
      if scorecard.present?
        return t(".Incomplete")
      else
        quiz = Quiz.find quiz_id
        return quiz.level
      end
    else
      return Quiz::LEVEL[:easy]
    end
  end

	def get_quiz_stat(scorecard_id)
		stat = {}
		scorecard = Scorecard.find scorecard_id
		reports = scorecard.reports.joins(:answer)
		stat[:correct] = reports.where("answers.correct = ?", true).count
		stat[:incorrect] = reports.where("answers.correct = ?", false).count
		return stat
	end


  def get_report_label_color(question_id, quiz_id)
    if current_user.scorecards.present?
      scorecard = current_user.scorecards.where(quiz_id: quiz_id).last
      report = scorecard.reports.where(question_id: question_id).last if scorecard.present?
      return rescue nil if report.blank?
      return report.answer.correct ? "correct-green" : "incorrect-red"
    end
  end


  def get_report_label_for_challenge(question_id)
    report = @reports.where(question_id: question_id).last
    return rescue nil if report.blank?
    return report.answer.correct ? "correct-green" : "incorrect-red"
  end
end
