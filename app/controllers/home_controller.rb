class HomeController < ApplicationController
  before_action :authenticate_user

  def dashboard
    @faq_category = FaqCategory.first
    @faq_questions = @faq_category.faq_questions
    @categories = Category.active.order('id asc')
    @quizzes = @categories.first.quizzes.active if @categories.present?
    @user_reviews = UserReview.all
  end

  def leader_board
    @scorecards = Scorecard.get_leader_board(params[:filter])
  end

  def update_locale
    redirect_back fallback_location: root_path
  end

end