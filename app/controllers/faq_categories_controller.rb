class FaqCategoriesController < ApplicationController


  def get_questions
 		faq_category = FaqCategory.find_by_id(params[:id])
 		@faq_questions = faq_category.faq_questions if faq_category.present?
 		render json: { faq_questions: @faq_questions, faq_category: faq_category, status: 200 }
  end

end
