class MembershipsController < ApplicationController
  before_action :authenticate_user, only: :create
  skip_before_action :verify_authenticity_token, only: [:stripe_webhook, :paypal_webhook]

  def index
    @user_plan = current_user.subscribed_to_plan
    @plans = Plan.all.order(:created_at)
  end

  def new
    @plan = Plan.find(params[:plan_id])
    @token = @@gateway.client_token.generate
  end

  def create
    if current_user.create_subscription(params)
      redirect_to root_path
    else
      redirect_to new_membership_path(plan_id: params[:plan_id])
    end
  end

  def stripe_webhook
    begin
      event_json = JSON.parse(request.body.read)
      event_object = event_json['data']['object']

      if event_json['type'] == 'customer.subscription.deleted'
        user = User.find_by(stripe_subscription_id: event_object["id"])
        user.update_attributes(stripe_subscription_id: nil)
        render body: nil, status: 200
      end
    rescue Exception => ex
      render body: nil, status: 400
    end
  end

  def paypal_webhook
    begin
      webhook_notification = @@gateway.webhook_notification.parse(
        request.params["bt_signature"],
        request.params["bt_payload"]
      )
      
      if (webhook_notification.kind == ('subscription_canceled' || 'subscription_expired'))
        user = User.find_by(paypal_subscription_id: webhook_notification.subscription.id)
        user.update_attributes(paypal_subscription_id: nil)
        render body: nil, status: 200
      end
    rescue Exception => ex
      render body: nil, status: 400
    end
  end
end