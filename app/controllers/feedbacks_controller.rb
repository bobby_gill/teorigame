class FeedbacksController < ApplicationController

  def new
  	@question = Question.find params[:question_id]
  	@answers = Answer.where(question_id: @question.id)
  	@feedback = Feedback.new
  end

  def create
  	@feedback = Feedback.create(params_feedbacks)
  end
 
  private 
    def params_feedbacks
      params.require(:feedback).permit(:description, :user_name, :email, :question_id, :answer_ids => [])   
    end

end