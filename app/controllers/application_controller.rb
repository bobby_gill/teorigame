class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale

  def after_sign_out_path_for(resource)
    root_path
  end

  def after_sign_in_path_for(resource)
    root_path
  end

  def default_url_options(options={})
    { :locale => I18n.locale }
  end

  def authenticate_user
    @current_user = current_user.present? ? current_user : guest_user
  end

  def check_subscriptions
    quiz = Quiz.find params[:id]
    has_subscription = current_user.has_subscription?
    if quiz.quiz_type == "paid"
      unless @current_user.guest
        if @current_user.present?
          if has_subscription.blank?
            flash[:danger] = "Please subscribe to continue"
            redirect_to memberships_path
          end
        end
      end
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name])
  end

  private
    def set_locale
      I18n.locale = params[:locale] || I18n.default_locale
    end
end