class CategoriesController < ApplicationController
  before_action :authenticate_user

  def get_quizzes
    @category = Category.find(params[:category_id])
		@quizzes = @category.quizzes.active if @category.present?
  end
end
