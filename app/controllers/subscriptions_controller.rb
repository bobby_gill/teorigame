class SubscriptionsController < ApplicationController
  before_action :authenticate_user, only: :create
  skip_before_action :verify_authenticity_token, only: :webhook

  def index
    @plan = Stripe::Plan.all.data
  end

  def new
    plan = Stripe::Plan.retrieve(params[:plan_id])
    @amount = plan.amount/100
  end

  def create
    if current_user.stripe_subscription_id.present?
      flash[:notice] = "Already Subscribed"
      redirect_to root_path and return
    end
    plan = Stripe::Plan.retrieve(params[:plan_id])
    stripe_customer = current_user.stripe_customer_id.present? ? Stripe::Customer.retrieve(current_user.stripe_customer_id) : Stripe::Customer.create(email: params[:stripeEmail], source: params[:stripeToken], description: 'Monthely Subscription')
    stripe_subscription = stripe_customer.subscriptions.create(:plan => plan.id)
    current_user.update_attributes(stripe_subscription_id: stripe_subscription.id, stripe_customer_id: stripe_customer.id)

    redirect_to root_path
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end

end
