class QuizzesController < ApplicationController
  before_action :authenticate_user
  before_action :check_subscriptions, :except => [:challenge_bank]

  def show
    @quiz = Quiz.find(params[:id])
    has_subscription = current_user.has_subscription?

    if current_user.guest? && @quiz.quiz_type == "paid"
      flash[:danger] = 'You Need to SignUp and Subscribe first.'
      redirect_to root_path
    elsif (has_subscription.blank? && @quiz.quiz_type == "paid")
      flash[:danger] = 'You Need to Subscribe first.'
      redirect_to root_path
    else
      @questions = @quiz.questions.active
      @pass_score = @quiz.pass_score
      @passing_percent = (@pass_score * 100)/ (@questions.count) if ( @pass_score.present? && @questions.present?)
      @mistakes_allowed = @questions.count - @pass_score
      @scorecard = @current_user.scorecards.find_by_quiz_id(@quiz.id)
      @reports = @scorecard.present? ? @scorecard.reports : []
      if @scorecard.present?
        @question = @questions.order('id asc').where.not('id in (?)', @reports.pluck(:question_id)).first if @questions.present?
      else
        @question = @questions.first
      end

      @wrong_answer_reports = Report.joins(:scorecard, :answer).where("scorecards.user_id = ? and answers.correct = ?", @current_user.id, false)
      @challeng_questions = Question.where(id: @wrong_answer_reports.pluck(:question_id))
      @right_answer = @reports.joins(:answer).where("answers.correct = ?", true).count if @reports.present?
      @wrong_answer = @reports.joins(:answer).where("answers.correct = ?", false).count if @reports.present?
      @percentage = (@right_answer * 100)/(@questions.count) if @right_answer.present?
      
      @scorecard.update_scorecard if @reports.pluck(:question_id).uniq.count == @questions.count
    end
  end


  def attempted_questions
    @quiz = Quiz.find(params[:id])
    @all_questions = @quiz.questions.active
    @scorecard = current_user.scorecards.find_by_quiz_id(@quiz.id)
    @reports = @scorecard.present? ? @scorecard.reports : []
    @attempted_question = Question.find(params[:question_id])

    if @attempted_question.reports.present?
      @question = @attempted_question
      @report = @scorecard.reports.where(question_id: @question.id).last if @question.present?
    else
      @question = @all_questions.order('id asc').where.not('id in (?)', @reports.pluck(:question_id)).first if @questions.present?
    end
    @wrong_answer_reports = Report.joins(:scorecard, :answer).where("scorecards.user_id = ? and answers.correct = ?", current_user.id, false)
    @challeng_questions = Question.where(id: @wrong_answer_reports.pluck(:question_id))
  end

  def destroy
    quiz = Quiz.find(params[:id])
    scorecard = quiz.scorecards.where(user_id: current_user.id).last
    scorecard.destroy if scorecard.present?
    redirect_to quiz_path(quiz.id)
  end


  def view_questions
    @quiz = Quiz.find(params[:id])
    @all_questions = @quiz.questions.active
    @scorecard = @quiz.scorecards.where(user_id: current_user.id).last
    @reports = @scorecard.reports

    if params[:step].present? && params[:step] == "next"
      @questions = @all_questions.order('id asc').where('id in (?)', @reports.pluck(:question_id))
      @question = @questions.where("id > ?", params[:question_id]).first
    elsif
    params[:question_id].present? && params[:step] == "previous"
      @questions = @all_questions.order('id desc').where('id in (?)', @reports.pluck(:question_id))
      @question = @questions.where("id < ?", params[:question_id]).first
    else
      @questions = @all_questions.order('id asc').where('id in (?)', @reports.pluck(:question_id))
      @question = @questions.first
    end
    @wrong_answer_reports = Report.joins(:scorecard, :answer).where("scorecards.user_id = ? and answers.correct = ?", current_user.id, false)
    @challeng_questions = Question.where(id: @wrong_answer_reports.pluck(:question_id)) if  @wrong_answer_reports.present?
    @report = @scorecard.reports.where(question_id: @question.id).last if @question.present?
  end



  def challenge_bank
    @reports = Report.joins(:scorecard, :answer).where("scorecards.user_id = ? and answers.correct = ?",current_user.id, false)
    @questions = Question.where(id: @reports.pluck(:question_id)).order('id asc')
    if @questions.present?
      if params[:qid].blank?
        @question = @questions.first
      else
        @current_question = Question.where(id: params[:qid]).last
        @next_question = @current_question.next(params[:qid])
        report = Report.where(question_id: @next_question.id).last if @next_question.present?
        if report.present? && !report.answer.correct
          @question = @questions.where(id: params[:qid]).last
          @question = @question.next(params[:qid])
        end
      end
    end
  end
end