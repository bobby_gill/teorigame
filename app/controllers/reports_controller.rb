class ReportsController < ApplicationController
  before_action :authenticate_user

  def create
    scorecard =  current_user.scorecards.where(quiz_id: params[:quiz_id]).last
    scorecard = current_user.scorecards.create(quiz_id: params[:quiz_id]) unless scorecard.present?
    report = scorecard.reports.find_or_create_by(question_id: params[:question_id], answer_id: params[:answer_id])
    wrong_answer = report.answer_id unless report.answer.correct
    right_answer = report.question.answers.find_by_correct(true).id
    render json: { right_answer: right_answer, wrong_answer: wrong_answer, status: 200 } 
  end

  def update_report
    report = Report.joins(:scorecard).where("scorecards.user_id = ? and reports.question_id = ?", current_user.id, (params[:question_id])).last
    report.update_attributes(answer_id: params[:answer_id])
    @reports = Report.joins(:scorecard, :answer).where("scorecards.user_id = ? and answers.correct = ?",current_user.id, false)
    @questions = Question.where(id: @reports.pluck(:question_id))
    @question = @questions.first if @questions.present?
    wrong_answer = report.answer_id unless report.answer.correct
    right_answer = report.question.answers.find_by_correct(true).id
    render json: { right_answer: right_answer, wrong_answer: wrong_answer, status: 200 }
  end
end
