ActiveAdmin.register Plan do
  permit_params :name, :description, :stripe_plan_id, :paypal_plan_id
  
  form do |f|
    f.inputs do
      f.input :name, :as => :string
      f.input :description, :as => :text
      f.input :stripe_plan_id, :as => :string
      f.input :paypal_plan_id, :as => :string
    end
    f.actions
  end
end
