ActiveAdmin.register Answer do

  form :title => proc{("New Answer")} do |f|
    f.inputs do
      f.input :question_id, label: ("Question"), :as => :select, :collection => Question.all.map{|u| ["#{u.title}", u.id]}
      f.input :title, label: ("Title")
      f.input :correct, label: ("Correct")
      f.input :active, label: ("Active")
    end
      f.actions do   
      f.action(:submit, label: ("Create Answer") )      
      f.cancel_link(:back)
    end
  end




  controller do
    def permitted_params
      params.permit answer: [:title, :correct, :active, :question_id]
    end
  end
end
