ActiveAdmin.register Scorecard do

  form :title => proc{("New Scorecard")} do |f|
    f.inputs do
      f.input :quiz_id, label: ("Quiz"), :as => :select, :collection => Quiz.all.map{|u| ["#{u.title}", u.id]}
      f.input :user_id, label: ("User"), :as => :select, :collection => User.all.map{|u| ["#{u.email}", u.id]}
    end
      f.actions do   
      f.action(:submit, label: ("Create Scorecard") )      
      f.cancel_link(:back)
    end
  end
  controller do
    def permitted_params
      params.permit scorecard: [:quiz_id, :user_id]
    end
  end
end

