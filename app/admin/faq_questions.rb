ActiveAdmin.register FaqQuestion do
  permit_params :title, :faq_category_id, :answer_detail

  index do
    selectable_column
    id_column
    column "Faq Category" do |faq_que|
      link_to(faq_que.faq_category.title, admin_faq_category_path(faq_que.faq_category.id))
    end
    column "Question", :title
    column "Answer", :answer_detail
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row "Faq Category" do |faq_que|
        link_to(faq_que.title, admin_faq_category_path(faq_que.faq_category.id))
      end
      row "Question" do |faq_que|
        faq_que.title
      end
      row "Answer" do |faq_que|
        faq_que.answer_detail
      end
      row :created_at
      row :updated_at
    end
  end


  form do |f|
    f.inputs do
      f.input :faq_category_id, as: :select, collection: FaqCategory.all.map{|cat| [cat.title, cat.id]}
      f.input :title, label: "Question"
      f.input :answer_detail, label: "Answer"
    end
    f.actions
  end

end
