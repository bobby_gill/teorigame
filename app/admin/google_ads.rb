ActiveAdmin.register GoogleAd do


  collection_action :create, method: :post do
    google_ad = GoogleAd.new(params_google_ads)
    if google_ad.save && params['google_ad']['attachments_attributes'].present?
      attachment =  google_ad.attachments.new(name: params['google_ad']['attachments_attributes']['0']['name'], attachment: params['google_ad']['attachments_attributes']['0']['attachment'])
      attachment.save
    end
    redirect_to admin_google_ad_path(google_ad.id)
  end

  member_action :update, method: :put do
	  google_ad = GoogleAd.find params[:id]
	  google_ad.update_attributes(params_google_ads)
	  if google_ad.save && params_attributes.present?
      attachment = google_ad.update_attributes(params_attributes)
    end
    redirect_to admin_google_ad_path(google_ad.id)
  end

  form :title => proc{("New Google Ad")} do |f|
    f.inputs do
      f.input :title, label: ("Title")
      f.input :description, label: ("Description")
      f.input :url, label: ("URL")
      
      f.has_many :attachments do |attachment|  
        attachment.input :name
        attachment.input :attachment, :type => :file
        attachment.input :_destroy, :as => :boolean, :required => false, :label => 'Remove' 
      end
    end
      f.actions do
      f.action(:submit, label: ("Create Google Ad") )      
      f.cancel_link(:back)
    end
  end

  show do
    attributes_table do
    	row :id
      row :title
      row :description
      row :url
      row "Attachments" do |q|
        q.attachments.each do |attachment|
          span do
            image_tag(attachment.attachment.url(:thumb))
          end
        end
      end
    end
  end



  controller do
    private 
    def params_google_ads
      params.require(:google_ad).permit(:description, :title, :url)   
    end
    def params_attributes
    	params.require(:google_ad).permit(attachments_attributes: [:id, :name, :attachment, :_destroy])
  	end
  end
end
