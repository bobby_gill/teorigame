ActiveAdmin.register Category do


  controller do
    def permitted_params
      params.permit category: [:name, :description, :active]
    end
  end

  form do |f|
	  f.inputs "Category" do
	    f.input :name, :as => :string
	    br
	    f.input :description, :as => :text
	    br
	    f.input :active, :as => :boolean
	  end
	  f.actions
	end

end
