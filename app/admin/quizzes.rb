ActiveAdmin.register Quiz do

  form :title => proc{("New Quiz")} do |f|
    f.inputs do
      f.input :category_id, label: ("Category"), :as => :select, :collection => Category.all.map{|u| ["#{u.name}", u.id]}
      f.input :title, label: ("Title")
      f.input :description, label: ("Description")
      f.input :pass_score,label: ("Pass Score")
      f.input :level, label: ("Level"), :as => :select, :collection => Quiz::LEVEL, include_blank: false
      f.input :quiz_type, label: ("Quiz Type"), :as => :select, :collection => Quiz::TYPE, include_blank: false
      f.input :active, label: ("Active")
    end
      f.actions do   
      f.action(:submit, label: ("Create Quiz") )      
      f.cancel_link(:back)
    end
  end


  controller do
    def permitted_params
      params.permit quiz: [:title, :description, :active, :pass_score, :quiz_type, :category_id, :lavel]
    end
  end

end
