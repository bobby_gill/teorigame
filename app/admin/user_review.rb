ActiveAdmin.register UserReview do
	permit_params :id, :user_id, :message

	form do |f|
	  f.inputs "User Review" do
	    f.input :user_id, as: :select, collection: User.all.collect{ |a| [(a.full_name.blank? ? a.email : a.full_name), a.id] }
	  	br
	    f.input :message, :as => :text
	    br
	  end
	  f.actions
	end

end
