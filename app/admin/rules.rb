ActiveAdmin.register Rule do


  collection_action :create, method: :post do
    rule = Rule.new(params_rules)
    if rule.save && params['rule']['attachments_attributes'].present?
      attachment =  rule.attachments.new(name: params['rule']['attachments_attributes']['0']['name'], attachment: params['rule']['attachments_attributes']['0']['attachment'])
      attachment.save
    end
    redirect_to admin_rule_path(rule.id)
  end

  member_action :update, method: :put do
  	rule = Rule.find params[:id]
  	rule.update_attributes(params_rules)
  	if rule.save && params_attributes.present?
      attachment = rule.update_attributes(params_attributes)
    end
    redirect_to admin_rule_path(rule.id)
  end

  form :title => proc{("New Rule")} do |f|
    f.inputs do
      f.input :title, label: ("Title")
      f.input :description, label: ("Description")
      
      f.has_many :attachments do |attachment|  
        attachment.input :name
        attachment.input :attachment, :type => :file
        attachment.input :_destroy, :as => :boolean, :required => false, :label => 'Remove' 
      end
    end
      f.actions do
      f.action(:submit, label: ("Create Rule") )      
      f.cancel_link(:back)
    end
  end

  show do
    attributes_table do
    	row :id
      row :title
      row :description
      row "Attachments" do |q|
        q.attachments.each do |attachment|
          span do
            image_tag(attachment.attachment.url(:thumb))
          end
        end
      end
    end
  end



  controller do
    private 
    def params_rules
      params.require(:rule).permit(:description, :title)
    end
    def params_attributes
    	params.require(:rule).permit(attachments_attributes: [:id, :name, :attachment, :_destroy])
  	end
  end
end

