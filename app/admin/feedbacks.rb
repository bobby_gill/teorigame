ActiveAdmin.register Feedback do


  controller do
    def permitted_params
      params.permit feedback: [:user_name, :description, :email, :question_id, :answer_id]
    end
  end

end
