ActiveAdmin.register FaqCategory do
  permit_params :title, :heading

  index do
    selectable_column
    id_column
    column :title
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs do
      f.input :title
    end
    f.actions
  end

end
