ActiveAdmin.register Question do
  collection_action :create, method: :post do
    question = Question.new(params_questions)
    if question.save && params['question']['attachments_attributes'].present?
      attachment =  question.attachments.new(name: params['question']['attachments_attributes']['0']['name'], attachment: params['question']['attachments_attributes']['0']['attachment'])
      attachment.save
    end
    if  params['question']['answers_attributes'].present?
      question.update_attributes(params_attributes)
    end
    redirect_to admin_question_path(question.id)
  end
  
  member_action :update, method: :put do
    question = Question.find params[:id]
    question.update_attributes(params_questions)
    if question.save && params_attributes.present?
      attachment = question.update_attributes(params_attributes)
    end
    redirect_to admin_question_path(question.id)
  end

  form :title => proc{("New Question")} do |f|
    f.inputs do
      f.input :quiz_id, label: ("Quiz"), :as => :select, :collection => Quiz.all.map{|u| ["#{u.title}", u.id]}
      f.input :title, label: ("Title")
      f.input :choice, label: ("Choice"), :as => :select, :collection => ['Single', 'Multiple']
      f.input :active, label: ("Active")
      f.input :hint, label: ("Hint")

      f.has_many :answers do |answer|  
        answer.input :title, label: ("Title")
        answer.input :correct, label: ("Correct")
        answer.input :active, label: ("Active")
        answer.input :_destroy, :as => :boolean, :required => false, :label => 'Remove' 
      end
      
      f.has_many :attachments do |attachment|  
        attachment.input :name
        attachment.input :attachment, :type => :file
        attachment.input :_destroy, :as => :boolean, :required => false, :label => 'Remove' 
      end
    end
      f.actions do
      f.action(:submit, label: ("Create Question") )      
      f.cancel_link(:back)
    end
  end

  show do
    attributes_table do
      row :id
      row :quiz_id
      row :title
      row :choice
      row :active
      row :hint
      row "Attachments" do |q|
        q.attachments.each do |attachment|
          span do
            image_tag(attachment.attachment.url(:thumb))
          end
        end
      end
    end


    panel "Answers" do
      table_for question.answers do
        column :id do |answer| link_to "#{answer.id}", admin_answer_path(answer.id) end
        column :title
      end
    end
  end

  controller do
    private 
    def params_questions
      params.require(:question).permit(:quiz_id, :title, :choice, :active, :hint)   
    end
    def params_attributes
      params.require(:question).permit(attachments_attributes: [:id, :name, :attachment, :_destroy], answers_attributes: [:id, :title, :active, :correct, :_destroy])
    end
  end

end
