# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  title       :text
#  question_id :integer
#  active      :boolean          default(FALSE)
#  correct     :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Answer < ApplicationRecord  
	translates :title
  belongs_to :question
  has_many :reports, dependent: :destroy
  
  scope :active, -> { where(active: true)}
end
