# == Schema Information
#
# Table name: faq_questions
#
#  id              :integer          not null, primary key
#  faq_category_id :integer
#  title           :text
#  answer_detail   :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class FaqQuestion < ApplicationRecord
	translates :title, :answer_detail
  belongs_to :faq_category
end
