class Plan < ApplicationRecord
  # Callbacks
  # before_create :create_plans_on_stripe_paypal

  # def create_plans_on_stripe_paypal
  #   stripe_plan = create_plan_on_stripe
  #   paypal_plan = create_plan_on_paypal
  # end

  # def create_plan_on_stripe
  #   products = Stripe::Product.list
  #   if products.count == 0
  #     product = Stripe::Product.create({
  #       name: 'Drive Exam Platform',
  #       type: 'service',
  #     })
  #   else
  #     product = products.first
  #   end
  #   plan = Stripe::Plan.create({
  #     currency: 'usd',
  #     interval: 'month',
  #     nickname: self.name,
  #     product: product.id,
  #     amount: ,
  #   })
  #   self.stripe_plan_id = plan.id
  # end

  # def create_plan_on_paypal
  #   self.paypal_plan_id = self.name.parameterize.underscore
  # end

  def get_stripe_plan
    Stripe::Plan.retrieve(stripe_plan_id)
  end
end