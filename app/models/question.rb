# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  title      :text
#  quiz_id    :integer
#  active     :boolean          default(FALSE)
#  choice     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  hint       :text
#

class Question < ApplicationRecord
	translates :title, :choice, :hint
  belongs_to :quiz
  has_many :answers, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :attachments, as: :attached_item, dependent: :destroy
  accepts_nested_attributes_for :attachments, :answers, reject_if: :all_blank, allow_destroy: true
  scope :active, -> { where(active: true).order('id asc')}

  def next id
    Question.where("id > ?", id).first
  end

end
