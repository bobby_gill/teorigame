# == Schema Information
#
# Table name: scorecards
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  quiz_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Scorecard < ApplicationRecord
  
  belongs_to :quiz
  belongs_to :user
  has_many :reports, dependent: :destroy

  scope :by_created_at_greater_than, -> (date){ includes(:user).where("created_at >= ?", date) }

  # Update wrong and correct answer.
  def update_scorecard
    if self.reports.present?
      wrong_ans = 0
      correct_ans = 0
      self.reports.each do |report|
        report.is_correct_answer? ? (correct_ans += 1) : (wrong_ans += 1)
      end
      score_per = (((correct_ans).to_f/(self.quiz.questions.count).to_f)*100).round(2)
      self.update_attributes(wrong_answer: wrong_ans, correct_answer: correct_ans, score_percentage: score_per)
    end  
  end


  def self.get_leader_board(time_period)
    case time_period
    when 'today'
      scorecards = by_created_at_greater_than(Time.zone.now.beginning_of_day)
    when 'week'
      scorecards = by_created_at_greater_than(1.week.ago)
    when 'month'
      scorecards = by_created_at_greater_than(1.month.ago)
    else
      scorecards = by_created_at_greater_than(Time.zone.now.beginning_of_day)
    end
    get_filterd_data(scorecards)
  end

  def self.get_filterd_data(scorecards) 
    grouped_scorecards = get_scorecard_group_by_user(scorecards)
    maped_scorecards = map_scorecards(grouped_scorecards)
    ordered_scorecards = maped_scorecards.sort_by! { |k| k['score_percentage'] }.reverse!
    ordered_scorecards
  end

  def self.get_scorecard_group_by_user(scorecards)
    scorecards.group_by{|scorecard| scorecard.user}
  end

  def self.map_scorecards(grouped_scorecards) 
    grouped_scorecards.map{|user, scorecards| {'user' => user, 'score_percentage' =>  scorecards.map{ |scorecard| scorecard.score_percentage.to_f}.sum.round(2)/scorecards.count}}
  end

  def self.get_score_percentage
    return 0 unless self.quiz.present?
    (((self.correct_answer).to_f/(self.quiz.questions.count).to_f)*100).round(2)
  end

end
