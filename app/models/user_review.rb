# == Schema Information
#
# Table name: user_reviews
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  message    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserReview < ApplicationRecord
	translates :message
	belongs_to :user
	validates :message, :presence => true
end
