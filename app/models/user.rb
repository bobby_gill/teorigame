# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  provider               :string
#  uid                    :string
#  stripe_customer_id     :string
#  stripe_subscription_id :string
#  guest                  :boolean          default(FALSE)
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :scorecards, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: %i[facebook google_oauth2]

  # Callbacks
  after_save :send_subscription_email_notification, if: -> {!self.guest && subscription_changed?} 

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.first_name = auth.info.name.split(' ')[0]
      user.last_name = auth.info.name.split(' ')[1]
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def create_subscription(params)
    if params[:stripeToken].present?
      create_stripe_subscription(params[:stripeToken], params[:plan_id])
    elsif params[:payment_method_nonce].present?
      create_paypal_subscription(params[:payment_method_nonce], params[:plan_id])
    else
      false
    end
  end

  def create_stripe_subscription(stripe_token, plan_id)
    plan = Plan.find(plan_id)
    return false unless plan.present?

    if self.stripe_customer_id.blank?
      stripe_customer = Stripe::Customer.create({
        email: self.email,
        source: stripe_token
      })
    else
      stripe_customer = Stripe::Customer.retrieve(self.stripe_customer_id)
    end

    subscription = stripe_customer.subscriptions.create(plan: plan.stripe_plan_id)

    self.stripe_customer_id = stripe_customer.id
    self.stripe_subscription_id = subscription.id
    self.subscription_ends_at = Time.at(subscription.current_period_end)
    self.save
  end

  def create_paypal_subscription(payment_method_nonce, plan_id)
    plan = Plan.find(plan_id)
    return false unless plan.present?

    if self.paypal_customer_id.blank?
      paypal_customer = @@gateway.customer.create(
        :email => self.email,
        :payment_method_nonce => payment_method_nonce
      )
      paypal_customer = paypal_customer.customer
    else
      paypal_customer = @@gateway.customer.find(self.paypal_customer_id)
    end

    # Create Subscription
    result = @@gateway.subscription.create(
      :payment_method_token => paypal_customer.paypal_accounts[0].token,
      :plan_id => plan.paypal_plan_id
    )

    self.paypal_customer_id = paypal_customer.id
    self.paypal_subscription_id = result.subscription.id
    self.subscription_ends_at = result.subscription.billing_period_end_date
    self.save
  end
  
  def has_subscription_on_paypal?
    paypal_subscription_id.present?
  end

  def has_subscription_on_stripe?
    stripe_subscription_id.present?
  end

  def has_subscription?
    has_subscription_on_stripe? || has_subscription_on_paypal?
  end

  def subscribed_to_plan
    return nil unless has_subscription?
    if has_subscription_on_paypal?
      plans = @@gateway.plan.all
      subscription = @@gateway.subscription.find(paypal_subscription_id)
      Plan.find_by(paypal_plan_id: subscription.plan_id)
    else
      subscription = Stripe::Subscription.retrieve(stripe_subscription_id)
      Plan.find_by(stripe_plan_id: subscription.plan.id)
    end
  end

  def subscription_changed?
    stripe_subscription_id_changed? || paypal_subscription_id_changed?
  end

  def send_subscription_email_notification
    case subscription_change
    when "new_subscription"
      UserMailer.new_subscription(self).deliver_now
    when "subscription_changed"
      UserMailer.subscription_changed(self).deliver_now
    when "subscription_cancelled"
      UserMailer.subscription_cancelled(self).deliver_now
    end
  end

  def subscription_change
    if ((saved_change_to_paypal_subscription_id[0] == nil rescue false) || (saved_change_to_stripe_subscription_id[0] == nil rescue false))
      "new_subscription"
    elsif ((saved_change_to_paypal_subscription_id[1] == nil rescue false) || (saved_change_to_stripe_subscription_id[1] == nil rescue false))
      "subscription_cancelled"
    elsif ((saved_change_to_paypal_subscription_id[0] != nil rescue false) || (saved_change_to_stripe_subscription_id[0] != nil rescue false))
      "subscription_changed"
    end
  end
end
