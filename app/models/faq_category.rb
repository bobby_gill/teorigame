# == Schema Information
#
# Table name: faq_categories
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  heading    :string
#

class FaqCategory < ApplicationRecord
	translates :title, :heading
  has_many :faq_questions
end
