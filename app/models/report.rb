# == Schema Information
#
# Table name: reports
#
#  id           :integer          not null, primary key
#  question_id  :integer
#  answer_id    :integer
#  scorecard_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Report < ApplicationRecord
	belongs_to :answer
	belongs_to :question
	belongs_to :scorecard

  validates :question_id, uniqueness: { scope: [:scorecard_id] }, presence: true

  def is_correct_answer?
  	question.answers.find_by_correct(true).id == answer_id
  end
end
