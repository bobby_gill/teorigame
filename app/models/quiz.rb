# == Schema Information
#
# Table name: quizzes
#
#  id          :integer          not null, primary key
#  title       :text
#  description :text
#  active      :boolean          default(FALSE)
#  pass_score  :integer
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  level       :string           default("Easy")
#  quiz_type   :string
#

class Quiz < ApplicationRecord
	translates :title, :description
  belongs_to :category
  has_many :questions, dependent: :destroy
  has_many :scorecards, dependent: :destroy

  LEVEL = {:easy => "Easy", :hard => 'Hard'}
  TYPE = ['paid', 'free']

  scope :active, -> { where(active: true)}
end
