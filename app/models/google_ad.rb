# == Schema Information
#
# Table name: google_ads
#
#  id          :integer          not null, primary key
#  title       :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  url         :string
#

class GoogleAd < ApplicationRecord
	translates :title, :description
  has_many :attachments, as: :attached_item, dependent: :destroy
  accepts_nested_attributes_for :attachments, reject_if: :all_blank, allow_destroy: true
end
