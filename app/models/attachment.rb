# == Schema Information
#
# Table name: attachments
#
#  id                 :integer          not null, primary key
#  attached_item_id   :integer
#  attached_item_type :string
#  name               :string
#  attachment         :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Attachment < ApplicationRecord
	translates :name
  mount_uploader :attachment, AttachmentUploader
  belongs_to :attached_item, polymorphic: true

end
