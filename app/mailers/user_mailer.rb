class UserMailer < ApplicationMailer

  def new_subscription(user)
    @user = user
    @plan_name = user.subscribed_to_plan.name
    mail(to: user.email, subject: 'New Subscription')
  end

  def subscription_changed(user)
    @user = user
    @plan_name = user.subscribed_to_plan.name
    mail(to: user.email, subject: 'Subscription changed')
  end

  def subscription_cancelled(user)
    @user = user
    mail(to: user.email, subject: 'Subscription Cancelled')
  end
end
