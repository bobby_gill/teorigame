// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require common
//= require_tree .

$(document).ready(function(){
    $(".alert" ).fadeOut(8000);
    var question_id = $("#current_question").val();
    $('#'+question_id).addClass('on-current');

    $(".squestion-"+question_id).unbind().click(function() {
        var self = $(this);
        var answer_id = self.data("answer");
        var quiz_id = $("#current_quiz").val();
        $('.check-answer label').addClass('disabled');
        $.ajax({
            url: "/reports",
            type: 'POST',
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            data: {quiz_id: quiz_id, question_id: question_id, answer_id: answer_id },
            success: function(response) {
                right_answer = response.right_answer;
                if(answer_id == right_answer){
                    self.addClass('correct-answer');
                    $("#"+question_id).removeClass('on-current');
                    $("#"+question_id).addClass('correct-green');
                }
                else{
                    self.addClass('wrong-answer');
                    $("#"+question_id).removeClass('on-current');
                    $("#answer_"+right_answer).addClass('correct-answer');
                    $("#answer_"+right_answer).css({
                        'padding' : '10px 55px 10px 0 !important'
                    });
                    $("#"+question_id).css({
                        'padding' : '10px 55px 10px 0 !important'
                    });
                    $("#"+question_id).addClass('incorrect-red');
                    $('.plus-count').css({ 'bottom' : '30px', 'opacity': '0', 'font-size' : '18px'});
                    var new_count = parseInt($('.change-bank-count').text()) + 1
                    $('.change-bank-count').text(new_count)
                }
                $('.next-button').removeClass('disabled');
            }
        })
    });

    $(".question_completed").unbind().click(function() {
        var self = $(this);
        var question_id = self.attr("id");
        var quiz_id = $("#current_quiz").val();
        $.ajax({
            url: "/quizzes/"+quiz_id+"/attempted_questions",
            type: 'GET',
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            data: {quiz_id: quiz_id, question_id :question_id},
            success: function(response) {
                attempted_question_id = response.attempted_question.id;
            }
        })
    });

    var question_id = $("#current_question").val();
    $(".challenge-question-"+question_id).unbind().click(function() {
        var self = $(this);
        var answer_id = self.attr("id");
        $.ajax({
            url: "/reports/"+answer_id+"/update_report",
            type: 'PATCH',
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            data: { question_id: question_id, answer_id: answer_id },
            success: function(response) {
                right_answer = response.right_answer;
                $('.next-button').removeClass('disabled');
                if(parseInt(answer_id) == right_answer){
                    self.addClass('correct-answer');
                    $("#"+question_id).removeClass('on-current');
                    $("#"+question_id).removeClass('incorrect-red');
                    $("#"+question_id).addClass('correct-green');
                    setTimeout(function(){
                        location.reload();
                    }, 100);
                }
                else{
                    self.addClass('wrong-answer');
                    $("#"+question_id).removeClass('on-current');
                    $("#"+question_id).addClass('incorrect-red');
                    $("#"+right_answer).addClass('correct-answer');
                }
            }
        })
    });
    $(".hint").click(function() {
        $(this).hide();
        $('#hint').show(200);
    });

    $("#stripe-option").prop('checked', true);

    $("#paypal-option").change( function() {
        if($(this).is(':checked')){
            $("#stripe-btn").hide();
            $("#paypal-button").show();
        }
    });
    $("#stripe-option").change( function() {
        if($(this).is(':checked')){
            $("#stripe-btn").show();
            $("#paypal-button").hide();
        }
    });
});