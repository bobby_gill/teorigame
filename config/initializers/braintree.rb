@@gateway = Braintree::Gateway.new(
  :environment => Rails.application.secrets.braintree_env.to_sym, 
  :merchant_id => Rails.application.secrets.braintree_merchant_id,
  :public_key => Rails.application.secrets.braintree_public_key,
  :private_key => Rails.application.secrets.braintree_private_key,
)