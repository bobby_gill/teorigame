Rails.application.routes.draw do

  scope ":locale", defaults: {locale: Rails.application.secrets.default_locale}, :path_prefix => '/:locale' do
    ActiveAdmin.routes(self)
  end
  
  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'users/omniauth_callbacks'}
  post "/stripe_webhook" => "memberships#stripe_webhook"
  post "/paypal_webhook" => "memberships#paypal_webhook"
  scope "(:locale)", locale: /ar|en|sv/, defaults: { locale: Rails.application.secrets.default_locale } do
    devise_for :admin_users, ActiveAdmin::Devise.config
    # ActiveAdmin.routes(self)
    devise_for :users, skip: :omniauth_callbacks
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root to: "home#dashboard"
    post "/hook" => "memberships#hook"

    resources :home do
      collection do
        put 'update_locale'
        get 'leader_board'
      end
    end

    resources :categories do
      collection do
        get 'get_quizzes'
      end
    end
    
    resources :feedbacks
    
    resources :faq_categories do
      member do
        get 'get_questions'
      end
    end
    
    resources :quizzes do
      member do
        get 'attempted_questions'
        get 'view_questions'
      end
      collection do
        get 'challenge_bank'
      end
    end
    
    resources :scorecards
    resources :subscriptions, only: [:new, :create, :index] do
      collection do
        post 'webhook'
      end
    end
    
    resources :reports do
      member do
        patch 'update_report'
      end
    end
    resources :memberships
  end
end
