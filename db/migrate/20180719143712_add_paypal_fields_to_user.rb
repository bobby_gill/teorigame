class AddPaypalFieldsToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :paypal_customer_id, :string
    add_column :users, :paypal_subscription_id, :string
  end
end
