class CreateTranslationTable < ActiveRecord::Migration[5.1]
  def up
    Quiz.create_translation_table!({ :title => :text, :description => :text}, {
          :migrate_data => true
        })
    Question.create_translation_table!({ :title => :text, :choice => :string, :hint => :text}, {
          :migrate_data => true
        })
    Attachment.create_translation_table!({ :name => :string}, {
          :migrate_data => true
        })
    Answer.create_translation_table!({ :title => :text}, {
          :migrate_data => true
        })
  end

  def down
  	Quiz.drop_translation_table! :migrate_data => true
  	Question.drop_translation_table! :migrate_data => true
  	Attachment.drop_translation_table! :migrate_data => true
  	Answer.drop_translation_table! :migrate_data => true
  end
end
