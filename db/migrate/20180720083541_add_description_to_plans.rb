class AddDescriptionToPlans < ActiveRecord::Migration[5.1]
  def change
    add_column :plans, :description, :string
  end
end
