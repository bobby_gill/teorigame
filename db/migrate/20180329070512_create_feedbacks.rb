class CreateFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
    	t.references :question, index: true
    	t.references :answer, index: true
    	t.string :user_name
    	t.string :email
    	t.text :description
      t.timestamps
    end
  end
end
