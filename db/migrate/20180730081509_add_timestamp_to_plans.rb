class AddTimestampToPlans < ActiveRecord::Migration[5.1]
  def change
    add_column :plans, :created_at, :datetime, null: false
    add_column :plans, :updated_at, :datetime, null: false
  end
end
