class AddSubscriptionEndsAtToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :subscription_ends_at, :datetime
  end
end
