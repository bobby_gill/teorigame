class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
    	t.references :question, index: true
    	t.references :answer, index: true
    	t.references :scorecard, index: true
      
      t.timestamps
    end
  end
end
