class CreateAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :attachments do |t|
    	t.integer :attached_item_id
      t.string :attached_item_type
    	t.string :name
    	t.string :attachment, null: false

      t.timestamps
    end
  end
end
