class CreateMemberships < ActiveRecord::Migration[5.1]
  def self.up
    create_table :memberships do |t|
      t.references :user, index: true
      t.integer :plan_id
      t.string :paypal_invoice_id
      t.timestamp :start_date
      t.timestamp :expire_date
      t.integer :next_plan_id
      t.timestamps
    end
  end
  def self.down

  end
end
