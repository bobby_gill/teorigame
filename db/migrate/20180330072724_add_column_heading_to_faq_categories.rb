class AddColumnHeadingToFaqCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :faq_categories, :heading, :string
  end
end
