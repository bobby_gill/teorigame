class CreateFaqQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :faq_questions do |t|
      t.references :faq_category, index: true
      t.text :title
      t.text :answer_detail
      t.timestamps
    end
  end
end
