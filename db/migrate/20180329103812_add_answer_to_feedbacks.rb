class AddAnswerToFeedbacks < ActiveRecord::Migration[5.1]
  def change
  	add_column :feedbacks, :answer_ids, :text, array:true, default: []
  end
end
