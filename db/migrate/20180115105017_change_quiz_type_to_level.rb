class ChangeQuizTypeToLevel < ActiveRecord::Migration[5.1]
  def change
    remove_column :quizzes, :quiz_type
    add_column :quizzes, :level, :string, :default => Quiz::LEVEL[:easy]
  end
end
