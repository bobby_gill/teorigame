class AddColumnUrlToGoogleAds < ActiveRecord::Migration[5.1]
  def change
  	add_column :google_ads, :url, :string
  end
end
