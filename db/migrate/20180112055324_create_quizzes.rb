class CreateQuizzes < ActiveRecord::Migration[5.1]
  def change
    create_table :quizzes do |t|
      t.text :title
      t.text :description
      t.boolean :active, default: false
      t.integer :pass_score
      t.references :category, index: true

      t.timestamps
    end
  end
end