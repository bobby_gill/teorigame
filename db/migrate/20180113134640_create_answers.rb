class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.text :title
      t.references :question, index: true
      t.boolean :active, default: false
      t.boolean :correct, default: false

      t.timestamps
    end
  end
end
