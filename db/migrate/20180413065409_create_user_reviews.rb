class CreateUserReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :user_reviews do |t|
      t.integer :user_id
      t.text :message

      t.timestamps
    end
  end
end
