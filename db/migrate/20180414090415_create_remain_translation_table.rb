class CreateRemainTranslationTable < ActiveRecord::Migration[5.1]
  def up
  	Category.create_translation_table!({ :name => :string, :description => :text}, {
          :migrate_data => true
        })
  	FaqCategory.create_translation_table!({ :title => :text, :heading => :string}, {
          :migrate_data => true
        })
    GoogleAd.create_translation_table!({ :title => :text, :description => :text}, {
          :migrate_data => true
        })
    FaqQuestion.create_translation_table!({ :title => :text, :answer_detail => :text}, {
          :migrate_data => true
        })
    Rule.create_translation_table!({ :title => :text, :description => :text}, {
          :migrate_data => true
        })
    UserReview.create_translation_table!({ :message => :text}, {
          :migrate_data => true
        })
  end

  def down
  	Category.drop_translation_table! :migrate_data => true
  	FaqCategory.drop_translation_table! :migrate_data => true
  	GoogleAd.drop_translation_table! :migrate_data => true
  	FaqQuestion.drop_translation_table! :migrate_data => true
  	Rule.drop_translation_table! :migrate_data => true
  	UserReview.drop_translation_table! :migrate_data => true
  end
end
