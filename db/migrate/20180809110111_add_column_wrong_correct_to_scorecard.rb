class AddColumnWrongCorrectToScorecard < ActiveRecord::Migration[5.1]
  def change
    add_column :scorecards, :wrong_answer, :integer
    add_column :scorecards, :correct_answer, :integer
    add_column :scorecards, :score_percentage, :float
  end
end
