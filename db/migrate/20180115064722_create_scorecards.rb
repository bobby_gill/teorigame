class CreateScorecards < ActiveRecord::Migration[5.1]
  def change
    create_table :scorecards do |t|
      t.references :user, index: true
      t.references :quiz, index: true
      
      t.timestamps
    end
  end
end
