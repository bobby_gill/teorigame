class RemoveColumnAnswerFromFeedback < ActiveRecord::Migration[5.1]
  def change
    remove_column :feedbacks, :answer_id, :reference, index: true
  end
end
