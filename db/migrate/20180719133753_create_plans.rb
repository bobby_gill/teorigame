class CreatePlans < ActiveRecord::Migration[5.1]
  def change
    create_table :plans do |t|
      t.string :name
      t.string :stripe_plan_id
      t.string :paypal_plan_id
    end
  end
end
