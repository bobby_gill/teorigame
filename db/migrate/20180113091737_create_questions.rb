class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.text :title
      t.references :quiz, index: true
      t.boolean :active, default: false
      t.string :choice
      
      t.timestamps
    end
  end
end
