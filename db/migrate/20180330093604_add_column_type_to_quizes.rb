class AddColumnTypeToQuizes < ActiveRecord::Migration[5.1]
  def change
    add_column :quizzes, :quiz_type, :string
  end
end
