class AddOmniauthToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    remove_column :users, :age, :integer
    remove_column :users, :contact_number, :string
  end
end
